#-------------------------------------------------
#
# Project created by QtCreator 2015-06-19T11:46:24
#
#-------------------------------------------------

#QT       += core
#QT       += gui

TARGET = photometric
TEMPLATE = lib

DEFINES += PHOTOMETRIC_LIBRARY

release: DESTDIR = ../build/release
debug:   DESTDIR = ../build/debug

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.ui

HEADERS += \
    photometric.h \
    json/json.h \
    SrtmHgt.h \
    PhotoRotator.h

SOURCES += photometric.cpp \
    json/json.cpp \
    SrtmHgt.cpp \
    PhotoRotator.cpp

unix {
    target.path = /usr/lib
    INSTALLS += target
}

#===============================================
#photocoordinates

LIBS += -L$$DESTDIR
LIBS += -lCoordTransformer
LIBS += -lPhotoEarthCoord

#===============================================
#opencv2.4.9

INCLUDEPATH += ../libs/opencv/sources/include \
            ../libs/opencv/sources/modules/calib3d/include \
            ../libs/opencv/sources/modules/contrib/include \
            ../libs/opencv/sources/modules/core/include \
            ../libs/opencv/sources/modules/features2d/include \
            ../libs/opencv/sources/modules/flann/include \
            ../libs/opencv/sources/modules/highgui/include \
            ../libs/opencv/sources/modules/imgproc/include \
            ../libs/opencv/sources/modules/legacy/include \
            ../libs/opencv/sources/modules/ml/include \
            ../libs/opencv/sources/modules/objdetect/include \
            ../libs/opencv/sources/modules/photo/include \
            ../libs/opencv/sources/modules/video/include \

LIBS += -L../libs/opencv/lib_centos
LIBS += -lopencv_calib3d
LIBS += -lopencv_core
LIBS += -lopencv_features2d
LIBS += -lopencv_flann
LIBS += -lopencv_highgui
LIBS += -lopencv_imgproc

#===============================================
#proj
#install needed: yum install proj proj-devel

LIBS += -lproj
