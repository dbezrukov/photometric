#include "PhotoRotator.h"


PhotoRotator::PhotoRotator(QObject* parent)
    : QObject(parent)
{
}

PhotoRotator::~PhotoRotator()
{
}

bool PhotoRotator::convert(const double bboxMinLon, const double bboxMaxLon,
                           const double bboxMinLat, const double bboxMaxLat,
                           const double leftTopLon, const double leftTopLat,
                           const double rightTopLon, const double rightTopLat,
                           const double rightBottomLon, const double rightBottomLat,
                           const double leftBottomLon, const double leftBottomLat,
                           const QString& srcPhotoFileName,
                           const QString& dstPhotoFileName,
                           const int dst_width_pix, const int dst_height_pix)
{
    bool retVal = false;

    //log_debug(QString("geoBBox: MinLon=%1, MaxLon=%2, MinLat=%3, MaxLat=%4")
    //    .arg(QString::number(bboxMinLon, 'f', 7))
    //    .arg(QString::number(bboxMaxLon, 'f', 7))
    //    .arg(QString::number(bboxMinLat, 'f', 7))
    //    .arg(QString::number(bboxMaxLat, 'f', 7)));

    //log_debug(QString("GeoLeftTop: lon=%1, lat=%2")
    //    .arg(QString::number(leftTopLon, 'f', 7))
    //    .arg(QString::number(leftTopLat, 'f', 7)));

    //log_debug(QString("GeoRightTop: lon=%1, lat=%2")
    //    .arg(QString::number(rightTopLon, 'f', 7))
    //    .arg(QString::number(rightTopLat, 'f', 7)));

    //log_debug(QString("GeoRightBottom: lon=%1, lat=%2")
    //    .arg(QString::number(rightBottomLon, 'f', 7))
    //    .arg(QString::number(rightBottomLat, 'f', 7)));

    //log_debug(QString("GeoLeftBottom: lon=%1, lat=%2")
    //    .arg(QString::number(leftBottomLon, 'f', 7))
    //    .arg(QString::number(leftBottomLat, 'f', 7)));


    //1.--------------------------------------------------------------
    log_debug(QString("source image path: %1").arg(srcPhotoFileName));
    if (srcPhotoFileName.isEmpty()) {
        log_error("srcPhotoFileName is empty");
        return retVal;
    }

    //2.--------------------------------------------------------------
    log_debug(QString("dest image path: %1").arg(dstPhotoFileName));
    if (dstPhotoFileName.isEmpty()) {
        log_error("dstPhotoFileName is empty");
        return retVal;
    }

    QFile dstPhotoFile(dstPhotoFileName);
    if (dstPhotoFile.exists()) {
        if (!dstPhotoFile.remove()) {
            log_error(QString("can`t remove file  `%1`").arg(dstPhotoFileName));
            return retVal;
        }
    }

    //3.--------------------------------------------------------------
    //Read input image
    string str_srcPhotoFileName = srcPhotoFileName.toStdString();

    //---> jpg
    //Mat input = imread(str_srcPhotoFileName, CV_LOAD_IMAGE_COLOR);
    //---alpha---сохранненый в AdobePhotoshop c прозрачными пикселями
    //Mat input = imread(str_srcPhotoFileName, IMREAD_UNCHANGED);

    // Create mat with alpha channel
    
    Mat input0 = imread(str_srcPhotoFileName, IMREAD_UNCHANGED);

    if (!input0.data) {
    	log_error(QString("can't read image at path: %1").arg(srcPhotoFileName));
        return -1;
    }


    resize(input0, input0, cvSize(dst_width_pix, dst_height_pix));

    Mat input(input0.rows, input0.cols, CV_8UC4);

    for (int i = 0; i < input0.rows; ++i) {
        for (int j = 0; j < input0.cols; ++j) {
            Vec3b& rgb = input0.at<Vec3b>(i, j);

            Vec4b& rgba = input.at<Vec4b>(i, j);
            rgba[0] = rgb[0];
            rgba[1] = rgb[1];
            rgba[2] = rgb[2];
            rgba[3] = UCHAR_MAX;
        }
    }



    if (!input.data) {
        log_error(QString("can`t read photo `%1`").arg(srcPhotoFileName));
        return retVal;
    }

    //namedWindow("input window", CV_WINDOW_AUTOSIZE);
    //imshow("input window", input );

    //4.--------------------------------------------------------------
    Point2f inputQuad[4];
    inputQuad[0] = Point2f(0, 0); //src Left Top
    inputQuad[1] = Point2f(input.cols-1, 0); //src Right Top
    inputQuad[2] = Point2f(input.cols-1, input.rows-1); //src Right Bottom
    inputQuad[3] = Point2f(0, input.rows-1); //src Left Bottom

    //5.--------------------------------------------------------------
    Point2f outputQuad[4];

    //leftTop
    double x0 = getX_pix(leftTopLon,
                        bboxMinLon, bboxMaxLon,
                        dst_width_pix);

    double y0 = getY_pix(leftTopLat,
                        bboxMinLat, bboxMaxLat,
                        dst_height_pix);

    //log_debug(QString("x=%1, y=%2").arg(x).arg(y));

    //outputQuad[0] = Point2f(x, y); //dst Left Top

    //---------------
    //rightTop
    double x1 = getX_pix(rightTopLon,
                 bboxMinLon, bboxMaxLon,
                 dst_width_pix);

    double y1 = getY_pix(rightTopLat,
                 bboxMinLat, bboxMaxLat,
                 dst_height_pix);

    //log_debug(QString("x=%1, y=%2").arg(x).arg(y));

    //outputQuad[1] = Point2f(x, y); //dst Right Top

    //---------------
    //rightBottom
    double x2 = getX_pix(rightBottomLon,
                 bboxMinLon, bboxMaxLon,
                 dst_width_pix);

    double y2 = getY_pix(rightBottomLat,
                 bboxMinLat, bboxMaxLat,
                 dst_height_pix);

    //log_debug(QString("x=%1, y=%2").arg(x).arg(y));

    //outputQuad[2] = Point2f(x, y); //dst Right Bottom

    //---------------
    //leftBottom
    double x3 = getX_pix(leftBottomLon,
                 bboxMinLon, bboxMaxLon,
                 dst_width_pix);

    double y3 = getY_pix(leftBottomLat,
                 bboxMinLat, bboxMaxLat,
                 dst_height_pix);

    //log_debug(QString("x=%1, y=%2").arg(x).arg(y));

    //outputQuad[3] = Point2f(x, y); //dst Left Bottom


    outputQuad[0] = Point2f(x0, y0); //dst Left Top
    outputQuad[1] = Point2f(x1, y1); //dst Right Top
    outputQuad[2] = Point2f(x2, y2); //dst Right Bottom
    outputQuad[3] = Point2f(x3, y3); //dst Left Bottom





    //------------------------------------------------------
    //------------------------------------------------------
    //------------------------------------------------------
    //------------------------------------------------------
    //------------------------------------------------------
    //
    //TODO a.kozlov 2016.05.13.

    //photocoordinates - неверное рассчитывает координаты
    //и соответсвенно в PhotoEarthCoord

    //либо это ошибка в файле телеметрии
    //C:\PhotoCoord7_qt-gui.2016.05.12\PhotoTest1\data\27.860.tel


    outputQuad[2] = Point2f(x0, y0);
    outputQuad[3] = Point2f(x1, y1);
    outputQuad[0] = Point2f(x2, y2);
    outputQuad[1] = Point2f(x3, y3);



    //------------------------------------------------------
    //------------------------------------------------------
    //------------------------------------------------------
    //------------------------------------------------------
    //------------------------------------------------------










    //6.--------------------------------------------------------------
    // Lambda Matrix
    Mat lambda(2, 4, CV_32FC1);
    // Set the lambda matrix the same type and size as input
    lambda = Mat::zeros(input.rows, input.cols, input.type());

    //Get the Perspective Transform Matrix i.e. lambda
    lambda = getPerspectiveTransform(inputQuad, outputQuad);

    //Output Image;
    //Mat output;
    //Mat output(dst_height_pix, dst_width_pix, CV_8UC3, Scalar(0));
    //---alpha
    Mat output(input.rows, input.cols, input.type());




    //Apply the Perspective Transform just found to the src image
    warpPerspective(input, output, lambda, output.size());


    //namedWindow("output window", CV_WINDOW_AUTOSIZE);
    //imshow("output window", output );


    //7.--------------------------------------------------------------
    string str_dstPhotoFileName = dstPhotoFileName.toStdString();
    retVal = imwrite(str_dstPhotoFileName, output);



    //8.--------------------------------------------------------------
    //освобождение памяти
    input0.release();
    input.release();
    output.release();
    lambda.release();

    return retVal;
}

double PhotoRotator::getX_pix(const double lon,
                              const double lon1, const double lon2,
                              const int width_pix)
{
    double retVal = (lon - lon1)*((double)width_pix)/(lon2 - lon1);

    //---------TODO 2016.05.12 a.kozlov,
    //скорее всего для большей точности эти поправки вносить не надо,
    //перспективное искажение должно отработать с отрицательными координатами
    //и более точно
    if (retVal < 0.0) retVal = 0.0;
    if (retVal >= width_pix) retVal = width_pix - 1.0;

    return retVal;
}

double PhotoRotator::getY_pix(const double lat,
                              const double lat1, const double lat2,
                              const int height_pix)
{
    double retVal = (lat2 - lat)*((double)height_pix)/(lat2 - lat1);

    //---------TODO 2016.05.12 a.kozlov,
    //скорее всего для большей точности эти поправки вносить не надо,
    //перспективное искажение должно отработать с отрицательными координатами
    //и более точно
    if (retVal < 0.0) retVal = 0.0;
    if (retVal >= height_pix) retVal = height_pix - 1.0;

    return retVal;
}

void PhotoRotator::log_debug(const QString& s)
{
    std::cout << "Debug. " << s.toStdString() << std::endl;
}

void PhotoRotator::log_warning(const QString& s)
{
    std::cout << "Warning. " << s.toStdString() << std::endl;
}

void PhotoRotator::log_error(const QString& s)
{
    std::cout << "Error. " << s.toStdString() << std::endl;
}
