#ifndef PHOTOMETRIC_H
#define PHOTOMETRIC_H

#include <string>

namespace photometric {

	extern std::string getBBox(const std::string& paramsJson);
	extern std::string WGStoXY(const std::string& paramsJson);
	extern std::string XYtoWGS(const std::string& paramsJson);

}

#endif // PHOTOMETRIC_H














