#pragma once

#ifndef PHOTOROTATOR_H
#define PHOTOROTATOR_H

#include <QObject>
#include <QString>
#include <QFile>

#include <iostream>

#include "opencv/cv.h"
#include "opencv/highgui.h"

//#include "opencv2/core/core.hpp"
//#include "opencv2/features2d/features2d.hpp"
//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/calib3d/calib3d.hpp"
//#include "opencv2/nonfree/features2d.hpp"
//#include "opencv2/opencv.hpp"

#include <string>

#include <vector>
#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;


class PhotoRotator: public QObject
{
    Q_OBJECT

public:
    PhotoRotator(QObject* parent = 0);
    ~PhotoRotator();

    bool convert(const double bboxMinLon, const double bboxMaxLon,
                 const double bboxMinLat, const double bboxMaxLat,
                 const double leftTopLon, const double leftTopLat,
                 const double rightTopLon, const double rightTopLat,
                 const double rightBottomLon, const double rightBottomLat,
                 const double leftBottomLon, const double leftBottomLat,
                 const QString& srcPhotoFileName,
                 const QString& dstPhotoFileName,
                 const int dst_width_pix, const int dst_height_pix);

private:
    double getX_pix(const double lon,
                    const double lon1, const double lon2,
                    const int width_pix);

    double getY_pix(const double lat,
                    const double lat1, const double lat2,
                    const int height_pix);

    void log_debug(const QString& s);
    void log_warning(const QString& s);
    void log_error(const QString& s);

};

#endif //PHOTOROTATOR_H
