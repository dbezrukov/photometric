#include <QMap>
#include <QVariant>
#include <QScopedPointer>
#include <QApplication>

#include <iostream>
#include <math.h>

#include "photometric.h"
#include "json/json.h"

#include "../libs/photocoordinates/PhotoEarthCoord/PhotoRenderer.h"
#include "../libs/photocoordinates/CoordinatesTransformer/EpsgConverter.h"

#include "opencv/cv.h"
#include "opencv/highgui.h"

#include "opencv2/core/core.hpp"
//#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
//#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/legacy/legacy.hpp"

#include "SrtmHgt.h"
#include "PhotoRotator.h"

namespace photometric {

EpsgConverter epsgConverter;

int argc = 0;
char ** argv;
bool GUIenabled = false;

QApplication app(argc, argv, GUIenabled);

inline double fround(double n, unsigned d)
{
	return floor(n * pow(10., d) + .5) / pow(10., d);
}

QString getUTMCode(double lon)
{
	int zoneNumber = 0;

	if (lon < 0.0) {
		lon += 360.;
	}

	zoneNumber = (int)::ceil(fabs(lon)/6.0);
	
	if (0 == zoneNumber) {
	    zoneNumber = 1;
	}

	// if ((mpUtmWgs84N == mapProj) || (mpUtmWgs84S == mapProj)) {
	// 	rs += 30;
	// }

	if ( zoneNumber < 10 ) {
		return QString( "EPSG:2840%1" ).arg( zoneNumber );
	}
	else {
		return QString( "EPSG:284%1" ).arg( zoneNumber );
	}
}

std::string getBBox(const std::string& paramsJson)
{
	bool ok;

	QMap<QString, QVariant> params = QtJson::parse(QString::fromStdString(paramsJson), ok).toMap();

	if (!ok) {
		return std::string("{ error: \"invalid json\" }");
	}

	am::PhotoRenderer *photoRender = new am::PhotoRenderer();

	//1. set parameters

	//Photo
	QString photoPath = params["photoPath"].toString();
	int photoWidth = params["photoWidth"].toInt();
	int photoHeight = params["photoHeight"].toInt();

	//Camera
	double cameraWidth = params["cameraWidth"].toDouble();
	double cameraHeight = params["cameraHeight"].toDouble();
	double cameraF = params["cameraF"].toDouble();

	double cameraCxOffset = params["cameraCxOffset"].toDouble();
	double cameraCyOffset = params["cameraCyOffset"].toDouble();
	double cameraDelta_f = params["cameraDelta_f"].toDouble();
	double cameraK1 = params["cameraK1"].toDouble();
	double cameraK2 = params["cameraK2"].toDouble();
	double cameraP1 = params["cameraP1"].toDouble();
	double cameraP2 = params["cameraP2"].toDouble();
	double cameraK3 = params["cameraK3"].toDouble();

	//Telemetry
	double telLon = params["telLon"].toDouble();
	double telLat = params["telLat"].toDouble();
	double telHeight = params["telHeight"].toDouble();
	double telHeading = params["telHeading"].toDouble();
	double telPitch = params["telPitch"].toDouble();
	double telRoll = params["telRoll"].toDouble();

	// //2. get height form srtm hgt
	// short srtmElevation = 0;
	// SrtmHgt *srtmHgt = new SrtmHgt(this);
	// if (srtmHgt->initHgtDirectory("C:/AeromosaicData/hgt/patch")) {
	// 	srtmElevation = srtmHgt->getElevation(telLon, telLat);
	// 	ui->txtLog->appendPlainText(QString("srtm elevation = %1").arg(srtmElevation));
	// 	if (srtmElevation == ERROR_ELEVATION) {
	// 		srtmElevation = 0;
	// 	}
	// }
	// else {
	// 	ui->txtLog->appendPlainText("ERROR. HGT directory is not init.");
	// }
	// telHeight -= (double)srtmElevation;
	// ui->txtLog->appendPlainText(QString("telHeight = %1").arg(telHeight));
	// delete srtmHgt;

	//3. init photo
	photoRender->initPhoto(
		//Photo
		photoWidth, photoHeight,
		//Camera
		cameraWidth, cameraHeight, cameraF, 
		cameraCxOffset, cameraCyOffset, cameraDelta_f, 
		cameraK1, cameraK2, cameraP1, cameraP2, cameraK3,
		//Telemetry
		telLon, telLat, telHeight,
		telHeading, telPitch, telRoll 
	);

	//4. get geo bounding box
	am::LatLonBBox geoBBox = photoRender->getLatLonBBox();

	if (geoBBox.MinLon == ERROR_COORD) {
		return std::string("{ error: \"invalid minLon\" }");
	}


	am::LatLonPBPath geoPath = photoRender->getPhotoBorderPath();

	delete photoRender;
    photoRender = NULL;

    
    PhotoRotator *photoRotator = new PhotoRotator();

    QString rotatedPath = photoPath;
    rotatedPath = rotatedPath.replace(".jpg", "_rotated.png");

    int rotatedSideSize = 800;

    bool result = photoRotator->convert(geoBBox.MinLon, geoBBox.MaxLon,
                          geoBBox.MinLat, geoBBox.MaxLat,
                          geoPath.GeoLeftTop.x, geoPath.GeoLeftTop.y,
                          geoPath.GeoRightTop.x, geoPath.GeoRightTop.y,
                          geoPath.GeoRightBottom.x, geoPath.GeoRightBottom.y,
                          geoPath.GeoLeftBottom.x, geoPath.GeoLeftBottom.y,
                          photoPath, 
                          rotatedPath,
                          rotatedSideSize, rotatedSideSize);

	delete photoRotator;
    photoRotator = NULL;

	QVariantMap output;
	output["minLon"] = fround(geoBBox.MinLon, 14);
	output["maxLon"] = fround(geoBBox.MaxLon, 14);
	output["minLat"] = fround(geoBBox.MinLat, 14);
	output["maxLat"] = fround(geoBBox.MaxLat, 14);

	QByteArray outputJson = QtJson::serialize(output);
	
	return std::string(outputJson.data());
}

std::string WGStoXY(const std::string& paramsJson)
{
	bool ok;
	QMap<QString, QVariant> params = QtJson::parse(QString::fromStdString(paramsJson), ok).toMap();

	if (!ok) {
		return std::string("{ x: 0, y: 0 }");
	}

	double val1 = params["lon"].toDouble();
	double val2 = params["lat"].toDouble();

	QString epsgSrc = "EPSG:4326";
	QString epsgDest = getUTMCode(val1);

	//qDebug() << " src " << epsgSrc;
	//qDebug() << "dest " << epsgDest;

	//qDebug() << "val1 " << val1;
	//qDebug() << "val2 " << val2;

	epsgConverter.transform(epsgSrc, epsgDest, &val1, &val2);

	//qDebug() << "res val1 " << val1;
	//qDebug() << "res val2 " << val2;

	QVariantMap output;
	output["x"] = fround(val1, 14);
	output["y"] = fround(val2, 14);

	QByteArray outputJson = QtJson::serialize(output);
	
	return std::string(outputJson.data());
}

std::string XYtoWGS(const std::string& paramsJson)
{
	bool ok;
	QMap<QString, QVariant> params = QtJson::parse(QString::fromStdString(paramsJson), ok).toMap();

	if (!ok) {
		return std::string("{ lat: 0, lon: 0 }");
	}

	double val1 = params["x"].toDouble();
	double val2 = params["y"].toDouble();

	epsgConverter.transform("EPSG:28406", "EPSG:4326", &val1, &val2);

	QVariantMap output;
	output["lat"] = fround(val1, 14);
	output["lon"] = fround(val2, 14);

	QByteArray outputJson = QtJson::serialize(output);
	
	return std::string(outputJson.data());
}

}
