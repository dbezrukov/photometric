#pragma once

#ifndef SRTMHGT_H
#define SRTMHGT_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <limits>
#include <math.h>
#include <QDataStream>


//-32768
const short ERROR_ELEVATION_HGT = std::numeric_limits<short>::min();
//degree per pixel
const double DEG_PER_PIX_HGT = 1.0/1200.0;
//degree per half pixel
const double DEG_PER_HALF_PIX_HGT = 1.0/2400.0;
//number of pixels in width and height
const int SRTM_SIDE_SIZE_HGT = 1201;
//2 bytes in hgt file
const int SIZE_ELEVATION_HGT = 2;

//360 degree -> 2Пr = 2*3.14*6 378 137 = 40 075 016.69 meters
const double ERROR_COORDINATE_HGT = 1000000000.0;
const int ERROR_LONLAT_INDEX_HGT = -1;


class SrtmHgt : public QObject
{
    Q_OBJECT

public:
    SrtmHgt(QObject* parent = 0);
    ~SrtmHgt();

    bool initHgtDirectory(const QString& hgtDirPath);

    short getElevation(const double lon, const double lat);

    /**
     * @brief copyHgtByRect - before call `initHgtDirectory` to set srcHgtDirPath
     * @param dstHgtDirPath - result directory
     * @param minLon - left longitude
     * @param maxLon - right longitude
     * @param minLat - down latitude
     * @param maxLat - top latitude
     * @return true if success
     */
    bool copyHgtByRect(const QString& dstHgtDirPath,
                       const double minLon, const double maxLon,
                       const double minLat, const double maxLat);

private:
    QString m_hgtDirPath;
    bool m_isHgtList;
    QFile m_hgtFileWindow;
    QDataStream m_hgtStreamWindow;

    int m_srtmWindowBottomLat; //bottom -> N60E030 -> Lat=60
    int m_srtmWindowLeftLon;   //left -> N60E030 -> Lon=30
    double m_shiftSrtmWindowTopLat;  //top -> N60+1+DEG_PER_HALF_PIX_HGT
    double m_shiftSrtmWindowLeftLon; //left -> E030-DEG_PER_HALF_PIX_HGT

    bool openSrtmHgtFile(const double lon, const double lat);
    short readHgtElevation(const double lon, const double lat);

    bool make_Dir1(const QString& dirPath);
    bool make_Dir(const QString& dirPath);
    bool remove_Dir(const QString& dirPath);
    bool copy_File(const QString& srcFileName, const QString& dstFileName);

    bool isCorrectPoint(const double lon, const double lat);
    int getLonIndex(const double lon);
    int getLatIndex(const double lat);
    QString getLonCellName(const double lon);
    QString getLatCellName(const double lat);

    // N59E029.hgt
    // N60E030.hgt
    QString getHgtName(const double lon, const double lat);

    // O/35/N59E029.hgt
    // P/36/N60E030.hgt
    QString getHgtHalfPathFileName(const double lon, const double lat);

    void log_error(QString s);
    void log_warning(QString s);
};

#endif //SRTMHGT_H
