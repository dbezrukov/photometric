#include "SrtmHgt.h"

#include <iostream>


SrtmHgt::SrtmHgt(QObject* parent)
    : QObject(parent)
    , m_hgtDirPath("")
    , m_isHgtList(false)
    , m_srtmWindowBottomLat((int)ERROR_COORDINATE_HGT)
    , m_srtmWindowLeftLon((int)ERROR_COORDINATE_HGT)
    , m_shiftSrtmWindowTopLat(ERROR_COORDINATE_HGT)
    , m_shiftSrtmWindowLeftLon(ERROR_COORDINATE_HGT)
{
}

SrtmHgt::~SrtmHgt()
{
    if (m_hgtFileWindow.isOpen()) {
        m_hgtFileWindow.close();
    }
}

bool SrtmHgt::initHgtDirectory(const QString& hgtDirPath)
{
    m_hgtDirPath = "";

    if (hgtDirPath.isEmpty()) {
        log_error("SrtmHgt.initHgtDirectory. Hgt directory is empty.");
        return false;
    }

    QDir hgtDir(hgtDirPath);
    if (!hgtDir.exists()) {
        log_error(QString("SrtmHgt.initHgtDirectory. Hgt directory is not exists `%1`.").arg(hgtDirPath));
        return false;
    }

    //1 000 000 sheet
    QStringList dirList = hgtDir.entryList(QDir::Dirs | QDir::AllDirs | QDir::NoDotAndDotDot);
    if (dirList.count() > 0) {
        m_isHgtList = false;
    }
    else {
        QStringList hgtList = hgtDir.entryList(QDir::Files);
        if (hgtList.count() < 1) {
            log_error(QString("SrtmHgt.initHgtDirectory. Hgt directory is empty `%1`.").arg(hgtDirPath));
            return false;
        }
        m_isHgtList = true;
    }

    m_hgtDirPath = hgtDir.absolutePath();
    return true;
}

short SrtmHgt::getElevation(const double lon, const double lat)
{
    short retVal = ERROR_ELEVATION_HGT;

    //srtm hgt directory
    if (m_hgtDirPath.isEmpty()) {
        log_error("SrtmHgt.initHgtDirectory. Hgt directory is empty.");
        return false;
    }

    if (!openSrtmHgtFile(lon, lat)) {
        log_error(QString("SrtmHgt.getElevation. Can`t open file lon=%1, lat=%2").arg(lon).arg(lat));
        return retVal;
    }

    retVal = readHgtElevation(lon, lat);

    return retVal;
}

bool SrtmHgt::copyHgtByRect(const QString& dstHgtDirPath,
                            const double minLon, const double maxLon,
                            const double minLat, const double maxLat)
{
    //src hgt dir
    if (m_hgtDirPath.isEmpty()) {
        log_error("SrtmHgt.copyHgtByRect. Source hgt directory is not exists.");
        return false;
    }

    //dst hgt dir
    //remove old hgt dir
    if (!remove_Dir(dstHgtDirPath)) {
        log_error(QString("SrtmHgt.copyHgtByRect. Can`t remove destination directory `%1`.").arg(dstHgtDirPath));
        return false;
    }
    if (!make_Dir(dstHgtDirPath)) {
        log_error(QString("SrtmHgt.copyHgtByRect. Can`t make destination directory `%1`.").arg(dstHgtDirPath));
        return false;
    }

    //hgt matrix
    if (!isCorrectPoint(minLon, minLat)) {
        log_error("SrtmHgt.copyHgtByRect. Min lon,lat incorrect.");
        return false;
    }
    if (!isCorrectPoint(maxLon, maxLat)) {
        log_error("SrtmHgt.copyHgtByRect. Max lon,lat incorrect.");
        return false;
    }

    int lon1 = floor(minLon);
    int lat1 = floor(minLat);
    int lon2 = ceil(maxLon);
    int lat2 = ceil(maxLat);

    QString hgtFileName;
    QString hgtOutFileName;
    QString srcHgtFileName;
    QString dstHgtFileName;

    QDir dsthgtDir(dstHgtDirPath);

    for(int lon=lon1; lon<=lon2; ++lon) {
        for(int lat=lat1; lat<=lat2; ++lat) {
            if (!m_isHgtList) {
                hgtFileName = getHgtHalfPathFileName((double)lon, (double)lat);
                if (hgtFileName.isEmpty()) {
                    log_error(QString("SrtmHgt.copyHgtByRect. hgtFileName.isEmpty lon=%1, lat=%2").arg(lon).arg(lat));
                    continue;
                }
                hgtOutFileName = getHgtName((double)lon, (double)lat);
                if (hgtOutFileName.isEmpty()) {
                    log_error(QString("SrtmHgt.copyHgtByRect. hgtOutFileName.isEmpty lon=%1, lat=%2").arg(lon).arg(lat));
                    continue;
                }
                dstHgtFileName = dsthgtDir.absolutePath() + "/" + hgtOutFileName;
            }
            else {
                hgtFileName = getHgtName((double)lon, (double)lat);
                if (hgtFileName.isEmpty()) {
                    log_error(QString("SrtmHgt.copyHgtByRect. hgtFileName.isEmpty lon=%1, lat=%2").arg(lon).arg(lat));
                    continue;
                }
                dstHgtFileName = dsthgtDir.absolutePath() + "/" + hgtFileName;
            }

            srcHgtFileName = m_hgtDirPath + "/" + hgtFileName;

            if (!copy_File(srcHgtFileName, dstHgtFileName)) {
                log_error(QString("SrtmHgt.copyHgtByRect. Can`t copy src=`%1` to dst=`%2`").arg(srcHgtFileName).arg(dstHgtFileName));
                continue;
            }

        } //for lat
    } //for lon

    return true;
}

bool SrtmHgt::openSrtmHgtFile(const double lon, const double lat)
{
    int lonName = floor(lon);
    int latName = floor(lat);
    if ((m_srtmWindowLeftLon == lonName) &&
        (m_srtmWindowBottomLat == latName)) {
        //it`s current opened file
        return true;
    }

    //reset the old values
    m_srtmWindowLeftLon = (int)ERROR_COORDINATE_HGT;
    m_srtmWindowBottomLat = (int)ERROR_COORDINATE_HGT;

    m_shiftSrtmWindowLeftLon = ERROR_COORDINATE_HGT;
    m_shiftSrtmWindowTopLat = ERROR_COORDINATE_HGT;

    //open srtm hgt file by bottom left corner - this file name
    if (m_hgtFileWindow.isOpen()) {
        m_hgtFileWindow.close();
    }

    //srtm hgt file name
    QString coordFileName;
    if (m_isHgtList) {
        coordFileName = getHgtName(lon, lat);
    }
    else {
        coordFileName = getHgtHalfPathFileName(lon, lat);
    }
    if (coordFileName.isEmpty()) {
        return false;
    }
    QString hgtFileName = m_hgtDirPath + "/" + coordFileName;

    m_hgtFileWindow.setFileName(hgtFileName);
    if (!m_hgtFileWindow.exists()) {
        log_error(QString("SrtmHgt.openSrtmHgtFile. File is not exists `%1`").arg(hgtFileName));
        return false;
    }

    //open srtm hgt file
    if (!m_hgtFileWindow.open(QIODevice::ReadOnly)) {
        log_error(QString("SrtmHgt.openSrtmHgtFile. Can`t open file `%1`").arg(hgtFileName));
        return false;
    }

    m_hgtStreamWindow.setDevice(&m_hgtFileWindow);

    //remember left bottom corner
    m_srtmWindowLeftLon = lonName;
    m_srtmWindowBottomLat = latName;

    //left top corner shift on half pixel
    m_shiftSrtmWindowLeftLon = ((double)m_srtmWindowLeftLon) - DEG_PER_HALF_PIX_HGT;
    m_shiftSrtmWindowTopLat = ((double)m_srtmWindowBottomLat) + 1.0 + DEG_PER_HALF_PIX_HGT;

    return true;
}

short SrtmHgt::readHgtElevation(const double lon, const double lat)
{
    short retVal = ERROR_ELEVATION_HGT;

    double lonCol = (lon - m_shiftSrtmWindowLeftLon)/DEG_PER_PIX_HGT;
    double latRow = (m_shiftSrtmWindowTopLat - lat)/DEG_PER_PIX_HGT;
    int col = floor(lonCol);
    int row = floor(latRow);

    if ((col < 0) || (col >= SRTM_SIDE_SIZE_HGT)) {
        return retVal;
    }
    if ((row < 0) || (row >= SRTM_SIDE_SIZE_HGT)) {
        return retVal;
    }

    qint64 eleOffset = SIZE_ELEVATION_HGT*(col + row*SRTM_SIDE_SIZE_HGT);
    if (!m_hgtFileWindow.seek(eleOffset)) {
        return retVal;
    }
    m_hgtStreamWindow >> retVal;

    return retVal;
}

bool SrtmHgt::make_Dir1(const QString& dirPath)
{
    if (dirPath.isEmpty()) {
        return false;
    }

    QDir dir(dirPath);
    if (dir.exists()) {
        return true;
    }

    if (!QDir().mkdir(dirPath)) {
        return false;
    }

    return true;
}

bool SrtmHgt::make_Dir(const QString& dirPath)
{
    QDir dir(dirPath);
    QString dirName = dir.absolutePath();
    QString dirName2 = dirName.replace("/", QDir::separator());

    QStringList dirNames = dirName2.split(QDir::separator());

    QString curDir = "";
    for( int i = 0; i < dirNames.count(); ++i ) {
        curDir = curDir + dirNames.at(i) + QDir::separator();
        if (!make_Dir1(curDir)) {
            return false;
        }
    }
    return true;
}

bool SrtmHgt::remove_Dir(const QString& dirPath)
{
    bool retVal = true;
    QDir dir(dirPath);

    if (dir.exists(dirPath)) {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
            if (info.isDir()) {
                retVal = remove_Dir(info.absoluteFilePath());
            }
            else {
                retVal = QFile::remove(info.absoluteFilePath());
            }

            if (!retVal) {
                return retVal;
            }
        }
        retVal = dir.rmdir(dirPath);
    }

    return retVal;
}

bool SrtmHgt::copy_File(const QString& srcFileName, const QString& dstFileName)
{
    QFile srcFile(srcFileName);
    if (!srcFile.exists()) {
        log_error(QString("SrtmHgt.copy_File. Source file is not exists `%1`.").arg(srcFileName));
        return false;
    }

    //remove old-version file
    QFile dstDelFile(dstFileName);
    if (dstDelFile.exists()) {
        if (!dstDelFile.remove()) {
            log_error(QString("SrtmHgt.copy_File. Can`t remove file `%1`.").arg(dstFileName));
            return false;
        }
    }

    //copy file
    if (!QFile::copy(srcFileName, dstFileName)) {
        log_error(QString("SrtmHgt.copy_File. Can`t copy `%1` to `%2`.").arg(srcFileName).arg(dstFileName));
        return false;
    }

    return true;
}

bool SrtmHgt::isCorrectPoint(const double lon, const double lat)
{
    if ((lon < -180.0) || (lon > +180.0)) {
        return false;
    }

    if ((lat < -90.0) || (lat > +90.0)) {
        return false;
    }

    return true;
}

int SrtmHgt::getLonIndex(const double lon)
{
    int retVal = ERROR_LONLAT_INDEX_HGT;
    if ((lon < -180.0) || (lon > +180.0)) {
        return retVal;
    }

    double longitude = 180.0 + lon;
    double zoneIndex = longitude/6.0;
    retVal = floor(zoneIndex);
    retVal++;
    if (retVal == 61) {
        retVal = 60;
    }

    return retVal;
}

int SrtmHgt::getLatIndex(const double lat)
{
    int retVal = ERROR_LONLAT_INDEX_HGT;
    if ((lat < -90.0) || (lat > +90.0)) {
        return retVal;
    }
    if (lat < -88.0) {
        return 0;
    }

    double latitude = 88.0 + lat;
    double zoneIndex = latitude/4.0;
    retVal = floor(zoneIndex);
    retVal++;

    return retVal;
}

QString SrtmHgt::getLonCellName(const double lon)
{
    QString retVal = "";

    int lonIndex = getLonIndex(lon);
    if (lonIndex==ERROR_LONLAT_INDEX_HGT) {
        log_warning(QString("SrtmHgt.getLonCellName. Incorrect lon index. lon=%1").arg(lon));
        return retVal;
    }

    retVal += QString("%1").arg(lonIndex, 2, 10, QLatin1Char('0'));

    return retVal;
}

QString SrtmHgt::getLatCellName(const double lat)
{
    QString retVal = "";

    int latIndex = getLatIndex(lat);
    if (latIndex==ERROR_LONLAT_INDEX_HGT) {
        log_warning(QString("SrtmHgt.getLatCellName. Incorrect lat index. lat=%1").arg(lat));
        return retVal;
    }

    if (latIndex >=23) {
        latIndex -= 22;
    }
    else {
        retVal = "S";
        latIndex = 23 - latIndex;
    }

    //65=A, 66=B, ... 85=U
    int code = 64 + abs(latIndex);
    char symbol = (char)code;
    retVal += symbol;

    return retVal;
}

QString SrtmHgt::getHgtName(const double lon, const double lat)
{
    QString retVal = "";
    if (!isCorrectPoint(lon, lat)) {
        log_error(QString("SrtmHgtZip.getHgtName. Incorrect lon=%1, lat=%2").arg(lon).arg(lat));
        return retVal;
    }

    //N60E030.hgt
    //N00E072.hgt.zip
    int lonName = floor(lon);
    int latName = floor(lat);

    if (latName >= 0) {
        retVal += "N";
    }
    else {
        retVal += "S";
    }
    retVal += QString("%1").arg(latName, 2, 10, QLatin1Char('0'));

    //N35E000.hgt.zip
    if (lonName >= 0) {
        retVal += "E";
    }
    else {
        retVal += "W";
    }
    retVal += QString("%1").arg(lonName, 3, 10, QLatin1Char('0'));
    retVal += ".hgt";

    return retVal;
}

QString SrtmHgt::getHgtHalfPathFileName(const double lon, const double lat)
{
    QString retVal = "";

    //lon
    QString lonCellName = getLonCellName(lon);
    if (lonCellName.isEmpty()) {
        return retVal;
    }

    //lat
    QString latCellName = getLatCellName(lat);
    if (latCellName.isEmpty()) {
        return retVal;
    }

    QString hgtFileName = getHgtName(lon, lat);
    if (hgtFileName.isEmpty()) {
        return retVal;
    }

    retVal = latCellName + "/" + lonCellName + "/" + hgtFileName;

    return retVal;
}

void SrtmHgt::log_error(QString s)
{
    std::cout << "Error." << s.toStdString() << std::endl;
}

void SrtmHgt::log_warning(QString s)
{
    std::cout << "Warning." << s.toStdString() << std::endl;
}

