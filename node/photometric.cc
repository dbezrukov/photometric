#include <node.h>

#include "../src/photometric.h"
#include <string>

namespace photometric {

using v8::Exception;
using v8::FunctionCallbackInfo;
using v8::Isolate;
using v8::HandleScope;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::String;
using v8::Value;

/*
void getInt(const FunctionCallbackInfo<Value>& args) {
	Isolate* isolate = Isolate::GetCurrent();
	HandleScope scope(isolate);

	if (args.Length() < 2) {
	  	isolate->ThrowException(Exception::TypeError(
	        String::NewFromUtf8(isolate, "Wrong number of arguments")));
	    return;
	}

	if (!args[0]->IsNumber() || !args[1]->IsNumber()) {
	    isolate->ThrowException(Exception::TypeError(
	        String::NewFromUtf8(isolate, "Wrong arguments")));
	    return;
	}

	//double value = args[0]->NumberValue() + args[1]->NumberValue();
	double value  = getInt();
	Local<Number> num = Number::New(isolate, value);

	args.GetReturnValue().Set(num);
}
*/

void getBBox(const FunctionCallbackInfo<Value>& args) {
  	Isolate* isolate = args.GetIsolate();
  	HandleScope scope(isolate);

  	if (args.Length() != 1) {
	  	isolate->ThrowException(Exception::TypeError(
	        String::NewFromUtf8(isolate, "Wrong number of arguments")));
	    return;
	}

	if (!args[0]->IsString()) {
	    isolate->ThrowException(Exception::TypeError(
	        String::NewFromUtf8(isolate, "Wrong arguments")));
	    return;
	}

	String::Utf8Value params(args[0]->ToString());
	std::string jsonOutput = photometric::getBBox(std::string(*params));

  	args.GetReturnValue().Set(String::NewFromUtf8(isolate, jsonOutput.c_str()));
}

void WGStoXY(const FunctionCallbackInfo<Value>& args) {
  	Isolate* isolate = args.GetIsolate();
  	HandleScope scope(isolate);

  	if (args.Length() != 1) {
	  	isolate->ThrowException(Exception::TypeError(
	        String::NewFromUtf8(isolate, "Wrong number of arguments")));
	    return;
	}

	if (!args[0]->IsString()) {
	    isolate->ThrowException(Exception::TypeError(
	        String::NewFromUtf8(isolate, "Wrong arguments")));
	    return;
	}

	String::Utf8Value params(args[0]->ToString());
	std::string jsonOutput = photometric::WGStoXY(std::string(*params));

  	args.GetReturnValue().Set(String::NewFromUtf8(isolate, jsonOutput.c_str()));
}

void XYtoWGS(const v8::FunctionCallbackInfo<Value>& args) {
  	Isolate* isolate = args.GetIsolate();
  	HandleScope scope(isolate);

  	if (args.Length() != 1) {
	  	isolate->ThrowException(Exception::TypeError(
	        String::NewFromUtf8(isolate, "Wrong number of arguments")));
	    return;
	}

	if (!args[0]->IsString()) {
	    isolate->ThrowException(Exception::TypeError(
	        String::NewFromUtf8(isolate, "Wrong arguments")));
	    return;
	}

	String::Utf8Value params(args[0]->ToString());
	std::string jsonOutput = photometric::XYtoWGS(std::string(*params));

  	args.GetReturnValue().Set(String::NewFromUtf8(isolate, jsonOutput.c_str()));
}

void Init(Local<Object> exports) {
  	NODE_SET_METHOD(exports, "getBBox", getBBox);
  	NODE_SET_METHOD(exports, "WGStoXY", WGStoXY);
  	NODE_SET_METHOD(exports, "XYtoWGS", XYtoWGS);
  	
  	/*
  	Isolate* isolate = Isolate::GetCurrent();

  	exports->Set(String::NewFromUtf8(isolate, "getBBox"), FunctionTemplate::New(isolate, getBBox)->GetFunction());
  	*/
  	// exports->Set(String::NewFromUtf8(isolate, "WGStoXY"), FunctionTemplate::New(isolate, WGStoXY)->GetFunction());
  	// exports->Set(String::NewFromUtf8(isolate, "XYtoWGS"), FunctionTemplate::New(isolate, XYtoWGS)->GetFunction());
}

NODE_MODULE(photometric, Init)

} // namespace photometric






