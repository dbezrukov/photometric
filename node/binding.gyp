{
  "targets": [
    {
      "target_name": "photometric",
      "sources": [ "photometric.cc" ],
      "include_dirs": [ ],
      "libraries": [
        "-lphotometric", 
        "-lCoordTransformer", 
        "-lPhotoEarthCoord", 
        "-L../../build/debug",

        "-lQtCore", 

        "-lopencv_calib3d",
		"-lopencv_core",
		"-lopencv_features2d",
		"-lopencv_flann",
		"-lopencv_highgui",
		"-lopencv_imgproc",
        "-L../../libs/opencv/lib_centos",

        "-lproj", 
        "-L../../libs/proj4/lib_centos"
      ],
    }
  ]
}
